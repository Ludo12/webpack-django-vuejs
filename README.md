# Virtualenv
- Installation de virtualenv
1. pip install virtualenv

- Virtualenv
1. virtualenv -p python3 env

- ACtivation de virtualenv
1. source env/bin/activate

## Installation de django
1. pip install django

- Création du projet
1. django-admin startproject project_name
2. cd project_name

- Vérification
1. python manage.py migrate
2. python manage.py runserver

- Template
1. Dans le dossier du project_name, on crée un nouveau dossier et un fichier
2. mkdir templates
3. cd templates
4. touch index.html
5. on code dans ce fichier une page minimaliste HTML avec dans le body un petit Hello Django

### Vuejs
- Création
1. A la racine du project_name, on crée le l'app vuejs
2. vue create nom_dossier

- Vérification
1. cd nom_dossier
2. yarn serve ou npm run server

### Configuration Projet
- Templates
1. on ouvre le projet avec l'IDE
2. on modifie le fichier project_name/urls.py

<pre>
    <code>
    from django.contrib import admin
    from django.urls import path

    from django.views.generic import TemplateView

    urlpatterns = [
        path('admin/', admin.site.urls),
        path('', TemplateView.as_view(template_name='index.html'), name='index')
    ]
    </code>
</pre>

3. on modifie le fichier project_name/setting.py

<pre>
    <code>
    TEMPLATES = [
        ...
        'DIRS': [
            str(BASE_DIR.joinpath('templates'))
        ],
        ...
    ]
    </code>
</pre>

#### Configuration et installation Webpack
- Webpack tracker
1. Dans le terminal, au niveau du dossier vuejs (nom_dossier)
2. npm install --save-dev webpack-bundle-tracker@0.4.3

- Webpack loader
1. Dans le terminal, à la racine du projet
2. pip install django-webpack-loader

- setting.py

<pre>
    <code>
    INSTALLED_APPS = [
        ...
        'webpack_loader',
        ...
    ]
    </code>
</pre>

en fin de fichier

<pre>
    <code>
    WEBPACK_LOADER = {
        'DEFAULT': {
            'CACHE': not DEBUG,
            'BUNDLE_DIR_NAME': 'webpack_bundles/',  # must end with slash
            'STATS_FILE': str(BASE_DIR.joinpath('frontend', 'webpack-stats.json')),
            'POLL_INTERVAL': 0.1,
            'TIMEOUT': None,
            'IGNORE': [r'.+\\.hot-update.js', r'.+\\.map'],
        }
    }
    </code>
</pre>    
    
- on crée le fichier vue.config.js dans le dossier vuejs(mon_dossier):

<pre>
    <code>
        const BundleTracker = require('webpack-bundle-tracker');
        module.exports = {
            publicPath: "http://0.0.0.0:8080",
            outputDir: "./dist/",
            chainWebpack: config => {
                config.optimization.splitChunks(false)
                config.plugin('BundleTracker').use(BundleTracker, [{
                    filename: './webpack-stats.json'
                }])
                config.resolve.alias.set('__STATIC__', 'static')
                config.devServer
                    .public('http://0.0.0.0:8080')
                    .host('0.0.0.0')
                    .port(8080)
                    .hotOnly(true)
                    .watchOptions({
                        poll: 1000
                    })
                    .https(false)
                    .headers({
                        'Access-Control-Allow-Origin': ['\*']
                    })
            }
        };
    </code>
</pre>

- index.hatml(templates)
1. on ajoute le render bundle de webpack, le fichier doit ressembler à ceci :

<pre><code><span class="xml">    </span><span class="hljs-template-tag">{% <span class="hljs-name"><span class="hljs-name">load</span></span> render_bundle from webpack_loader %}</span><span class="xml">
    <span class="hljs-meta">&lt;!DOCTYPE html&gt;</span>
    <span class="hljs-tag">&lt;<span class="hljs-name">html</span> <span class="hljs-attr">lang</span>=<span class="hljs-string">"en"</span>&gt;</span>
    <span class="hljs-tag">&lt;<span class="hljs-name">head</span>&gt;</span>
        <span class="hljs-tag">&lt;<span class="hljs-name">meta</span> <span class="hljs-attr">charset</span>=<span class="hljs-string">"UTF-8"</span>&gt;</span>
        <span class="hljs-tag">&lt;<span class="hljs-name">meta</span> <span class="hljs-attr">name</span>=<span class="hljs-string">"viewport"</span> <span class="hljs-attr">content</span>=<span class="hljs-string">"width=device-width, initial-scale=1.0"</span>&gt;</span>
        </span><span class="hljs-template-tag">{% <span class="hljs-name">render_bundle</span> 'app' 'css' %}</span><span class="xml">
        <span class="hljs-tag">&lt;<span class="hljs-name">title</span>&gt;</span>Document<span class="hljs-tag">&lt;/<span class="hljs-name">title</span>&gt;</span>
    <span class="hljs-tag">&lt;/<span class="hljs-name">head</span>&gt;</span>
    <span class="hljs-tag">&lt;<span class="hljs-name">body</span>&gt;</span>
        <span class="hljs-tag">&lt;<span class="hljs-name">h1</span>&gt;</span>Hello Django<span class="hljs-tag">&lt;/<span class="hljs-name">h1</span>&gt;</span>
        <span class="hljs-tag">&lt;<span class="hljs-name">div</span> <span class="hljs-attr">id</span>=<span class="hljs-string">"app"</span>&gt;</span><span class="hljs-tag">&lt;/<span class="hljs-name">div</span>&gt;</span>
        </span><span class="hljs-template-tag">{% <span class="hljs-name">render_bundle</span> 'app' 'js' %}</span><span class="xml">
    <span class="hljs-tag">&lt;/<span class="hljs-name">body</span>&gt;</span>
    <span class="hljs-tag">&lt;/<span class="hljs-name">html</span>&gt;</span></span>
</code></pre>

##### Server
1. A la racine du projet on lance le server : python manage.py runserver
2. Dans le dossier vuejs(mon_dossier) : yarn serve ou npm run server    